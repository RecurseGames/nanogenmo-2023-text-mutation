# Nanogenmo 2023 - Text Mutation

## Introduction

This repository contains a Node.js application which generates a ~50,000 word novel.
It is my entry for NaNoGenMo 2023. You can find details on my submission at
[https://github.com/NaNoGenMo/2023/issues/14](https://github.com/NaNoGenMo/2023/issues/14).

Example output: [They Become One Another](https://gitlab.com/RecurseGames/nanogenmo-2023-text-mutation/-/raw/main/output.pdf?ref_type=heads) (~62K words).

## Getting started

To generate a novel:

1. Install Node.js and NPM
1. Install dependencies (`npm install`)
1. Run app (`npm start`)
1. Novel will be output as a HTML file, output.html

## Notes

This has been put together in a bit of a rush, so apologies for the state of the code. Random fragments of abandoned experiments are scattered throughout. Only some of the final codebase is actually used to generate the novel. At a later point I may clean this up, but then again perhaps not.