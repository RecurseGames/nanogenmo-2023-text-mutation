import { getRandom } from "../frameworks/utilities/random";

const SURNAMES = [
    "Owen",
    "Morris",
    "Williams",
    "Solomon",
    "Michael",
    "Jones",
    "Humphry",
    "William",
    "Roberts",
    "Thomas",
    "Hughes",
    "Griffith",
    "Ishmel",
    "Willian",
    "Colfax",
    "Ower",
    "Hays",
    "Wnes",
    "Morgan",
    "Tyrer",
    "Gould",
    "Lewis",
    "Samuel",
    "Edwards",
    "Watkins",
    "Powell",
    "Bowen",
    "Oliver",
    "Price",
    "Davies",
    "Dais",
    "Evans",
    "Anderson",
    "Morgand",
    "Edward",
    "Jenkins",
    "James",
    "Jeffreys",
    "Parce",
    "Barclay",
    "Buckland",
    "Morgans",
    "Bromage",
    "Pritchard",
    "Rees",
    "Humphres",
    "Richards",
    "Joel",
    "Pryse",
    "Joshua",
    "Procter",
    "Mathias",
    "Emison",
    "Edmunds",
    "Watkin",
    "Griffiths",
    "Davis",
    "Howels",
    "Benjamin",
    "Harbert",
    "Layd",
    "Daires",
    "Riomas",
    "John",
    "Christopher",
    "Howell",
    "Hugh",
    "Badger",
    "Anthony",
    "Daniel",
    "David",
    "Robert",
    "Zaharia",
    "Evan",
    "Simon",
    "Bower",
    "Thos",
    "Danel",
    "Going",
    "Monis",
    "Gibbon",
    "Owens",
    "Pugh",
    "Darres",
    "Lloyd",
    "Roderick",
    "Iscion",
    "Ellis",
    "Cleaver",
    "Francis",
    "Hopkins",
    "Philips",
    "Phillips",
    "Norton",
    "Bonner",
    "Moyan",
    "Joseph",
    "Richard",
    "Gwynne",
    "Fafield",
    "Rowlands",
    "Prichard",
    "Humphrey",
    "Prichiart",
    "Walker",
    "Jane",
    "Morrice",
    "Smallowld",
    "Parry",
    "Larmond",
    "Wm",
    "Wynne",
    "Pierce",
    "Pary",
    "Bishop",
    "Heaton",
    "Ducker",
    "Salusbury",
    "Faulk",
    "Janes",
    "Bag",
    "Hughs",
    "Willians",
    "Pearson",
    "Adams",
    "Shone",
    "Butler",
    "Powel",
    "Leighton",
    "Brearton",
    "Lumb",
    "Hope",
    "McDonald",
    "Wms",
    "Holmes",
    "Gamewell",
    "Amos",
    "Stewart",
    "Bryan",
    "Durgan",
    "Scanderett",
    "Waters",
    "Crudge",
    "Lovel",
    "George",
    "Lidenton",
    "Comely",
    "Smith",
    "Moses",
    "Cuthbertson",
    "Harries",
    "Lovick",
    "Hopkin",
    "Elvery",
    "Cooke",
    "Moran",
    "Reece",
    "Watts",
    "Britton",
    "Harris",
    "Rhoderick",
    "Leyshon",
    "Jenkin",
    "Barber",
    "Archer",
    "Johnson",
    "Colchester",
    "Oldham",
    "Patterson",
    "Harvey",
    "Holland",
    "Crissell",
    "Outing",
    "Lawell",
    "Golding",
    "Garnham",
    "Marshall",
    "Streett",
    "Baram",
    "Wyatt",
    "Hodge",
    "Bolton",
    "Farley",
    "Cannon",
    "Bowler",
    "Foster",
    "Richeson",
    "Mercer",
    "Steetmen",
    "Whale",
    "Killick",
    "Grinstead",
    "Hodger",
    "Page",
    "Macbeth",
    "Blake",
    "Wilson",
    "Stevens",
    "Moorcroft",
    "Collens",
    "Pope",
    "Garrard",
    "Wood",
    "Ruglers",
    "Wray",
    "Woollord",
    "Dare",
    "Morphet",
    "Carter",
    "King",
    "Surman",
    "Goddard",
    "Holdon",
    "Kenter",
    "Laughon",
    "Seymour",
    "Brend",
    "Holder",
    "Grover",
    "Punnett",
    "Holtham",
    "Hobden",
    "Rogers",
    "Chandler",
    "England",
    "Macnish",
    "Eagle",
    "Tunsey",
    "Overiolers",
    "Trice",
    "Hewrn",
    "Webb",
    "Fairbanks",
    "McClatchie",
    "Ling",
    "Garret",
    "Martin",
    "Rottenberg",
    "Rearden",
    "Henley",
    "Pounceby",
    "Gardiner",
    "Ely",
    "Higham",
    "Nash",
    "Timmons",
    "Kemp",
    "Lucey",
    "Peters",
    "Chamberlin",
    "Ware",
    "Dodd",
    "Beckett",
    "Cooper",
    "Balls",
    "Halely",
    "Major",
    "Bennett",
    "Hutchings",
    "Brown",
    "Suttley",
    "Corbett",
    "Betchley",
    "Whichelow",
    "Pulsford",
    "Moates",
    "Rolfe",
    "Clements",
    "Higdon",
    "Jameson",
    "Hambridge",
    "Gorling",
    "Whithea",
    "Hodges",
    "Grisanthwaite",
    "Cuthbert",
    "Fenner",
    "Bassett",
    "Walton",
    "Mallphane",
    "Spencer",
    "Crop",
    "Gibbs",
    "Hunt",
    "Percey",
    "Skeet",
    "Guest",
    "Homes",
    "Palmer",
    "Mills",
    "Mears",
    "Taylor",
    "Banks",
    "Higgs",
    "Broch",
    "Stockley",
    "Whitty",
    "Meary",
    "Armstron",
    "Crockett",
    "Lock",
    "Stanchi",
    "Hurst",
    "Parke",
    "Wright",
    "Arway",
    "Carola",
    "Whitbread",
    "Newman",
    "Clifton",
    "Reynolds",
    "Bokan",
    "Sutton",
    "Hembree",
    "Lindle",
    "Cocking",
    "Hingley",
    "Hellman",
    "Russell",
    "Fax",
    "Soap",
    "Thornton",
    "Bull",
    "Harley",
    "Arnold",
    "Vaughan",
    "Grey",
    "Bodien",
    "Burgess",
    "Wiggell",
    "Hall",
    "Perce",
    "Shaw",
    "Flynn",
    "Stagg",
    "Cox",
    "Barwick",
    "Payne",
    "Slee",
    "Gerton",
    "Stephens",
    "Barrett",
    "Clarke",
    "Hart",
    "Tyrrell",
    "Evens",
    "Snelling",
    "Marsh",
    "Neve",
    "Renkin",
    "Giles",
    "Finkfill",
    "Goodall",
    "Dearlay",
    "Morriss",
    "Godemert",
    "Weed",
    "Bonte",
    "Joyner",
    "Arnott",
    "Cressy",
    "Mortimer",
    "Hatten",
    "Marriott",
    "Fraland",
    "Saunde",
    "Misson",
    "Wix",
    "Richarson",
    "Sanneman",
    "Redford",
    "Bunner",
    "Packer",
    "Finlayson",
    "Pech",
    "Ashneer",
    "Waterman",
    "Dourner",
    "Simkins",
    "Clay",
    "Tarpin",
    "Pople",
    "Elkins",
    "Sednell",
    "Wyles",
    "Covington",
    "Watter",
    "Dicker",
    "Farren",
    "Sills",
    "Gurney",
    "Dainton",
    "Kettle",
    "Mackey",
    "Ide",
    "Upjohn",
    "Sinker",
    "Furmidge",
    "Yanny",
    "Tonsfeldt",
    "Balen",
    "Abrahams",
    "Woodroffe",
    "Driver",
    "Clarson",
    "Thonfield",
    "Lonsdale",
    "Nichols",
];

export function getSurname(): string {
    return getRandom(SURNAMES);
}
