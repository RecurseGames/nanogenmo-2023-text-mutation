If you go to the mountains, walk around them. You will find paths with walls that are the mountain. And on the top of these walls are other paths, mountain paths. And on top of the mountain paths you will see mountain goats. Goats can walk on walls, you see.

Orbi was a goat. He had plain grey wool, and black horns. He spent his days eating grass on the top of the mountain. His favourites were milk grass and spiked lettuce. His tongue was thick and sharp, so the spiked lettuce did not hurt him. At night, Orbi would walk to the bottom of the mountain and through a wooden door. Inside was a small house. The goats lived there.

Now, if you visit the mountain you may see the door if you are lucky. Most adults cannot find it, which is why no-one knows where the goats sleep.

One day a large group of owls arrived at the mountain. They cracked the seeds of the trees and stripped their branches. When the sun rose, they laughed and shouted. And when the sun set, they sang and growled. The goats were upset by this, but they said nothing. They did not want to upset their new guests.
