Weak alliances are perhaps the most diverse. They invite value judgements, and fall into four broad categories.
Of the weak alliances, the worst alliances involve abuse.
If my partner has an abuser, but I remain neutral, I have totally failed in my care for them.
Slightly preferable is when my partner has an abuser who is hated by me. Either their abuser is my outright enemy, I am targetting them, or I am abusing the abuser myself. In all these cases though, I am burdened. My partner loves one who harms them, and I carry the weight of fighting on their behalf. At least my virtue may remain intact. Here it is worth clarifying the usage of the word abuse. Here it refers to a scenario where someone hates the person who loves them. In this context, it does not necessarily refer to the classic abusive relationship. Other possibilities include someone who has wronged, who I therefore must resist, but who remains committed to me. In this case the abuse is visited upon themselves, the result of a pathological system.
Finally there is the scenario where a third party is hated by one partner, while the other remains neutral.
In a strong alliance, there is an uninvolved third party, or a third party which both partners hate.
When partners interact with a stanger, the situation is semi-stable because if either develops a relationship with the stranger, the alliance may be disrupted.
When partners hate a third party, the situation is semi-stable due to conflict. The third party's opinions are not relevant. If they represent a common enemy, they are slightly better off. If an alliance targets a third party, but the third party does not hate both allies, the third party is less able to defend themselves.
Weak alliances cover a broad spectrum. When we see a weak relationship we usually make value judgements.
At best, one partner has an idol. This does not test the other partner's virtue, but it does test their vice. If they are jealous, the relationship may be jeopardised.
One notable scenario is when the third party hates one partner but loves the other.
Either the third party is constrained by their own internal conflict, or they may seek to break the alliance apart.
When someone has a tormenting relationship, but their partner is uninvolved, the alliance is weak. 
If my partner has an abuser, but the abuser is not my enemy, then I am allowing my partner to be harmed.
If my partner has a target, but the target is not my enemy, my reaction reflects my compassion, and what standard I hold my partner to.
If my partner has an idol, my own virtues are tested to a lesser degree. My vices, however, are not. If I am jealous, the relationship may be jeopardised.
In it's idealised form, the perfect alliance conjures the image of the married couple united by their faith in God. Though this feels dubious when viewed through a secular lense.
Toxic alliances are those where my partner and I have differing relationships with the same person, and abuse is present.
If my partner idolises my abuser, the relationship will be particularly harmful.
If my partner abuses my idol, my cognitive dissonance will be extreme.
If my partner abuses my abuser, this will produce the maximum harm, although less for me than if my partner idolised my abuser instead.
We are strangers.
Strange alliances occur when my partner and I have differing relationships with the same person, but abuse is not present.
If my partner targets someone who is targetting me, our alliance is weak.
If my partner idolises someone who in turn idolises me, we are exposed.
Finally, we discover the perfect alliance. This is the only alliance which is stable. It occurs when two partners have a common idol. Surely this fact has some spiritual significance.
Let's say our opinions of each other can be described as loving or hateful.
There is another type of weak alliance, where one partner has an abuser.
There is an additional type of weak alliance, one where I target someone who in turn targets my partner. This suggests an inability to alert my partner to their enemies, or perhaps that my partner is paying the price for my own aggression. This type of weak alliance is the most ambiguous.
When my idol hates my partner, or my partner hates my idol, our relationship is toxic. Toxic relationships exist on a spectrum.
The spectrum of toxic relationships is counterintuitive.
At best, my idol is my partner's enemy.
At best, my partner targets my idol.
This is the most stable triangle.
The other situations involving alliances are the most complex. 
This may jeopardise the relationship.
When partners have a common enemy, target, or abuser, the situation is semi-stable because conflict is inherently unstable.
When one partner has a target, again the situation is semi-table because conflict is present.
When partners interact with a stanger, the situation is semi-stable because if either develops a relationship with the stranger, conflict may occur.
In the latter case, we're looking at an alliance where one me
Slightly less stable is the V triad, where a person has two partners, but their partners do not have a relationship.
Even less stable would be a dysfunctional triad, where a person has two partners, but they have a hostile or tormenting relationship.
When we consider all the different combinations, we produce an interesting array of scenarios.
How might you descibe a situation where two people are partners, one of them has an abuser, and the other has no opinion of the abuser?
In a strong alliance, there is an uninvolved third party, or a third party which both partners have the same relationship with.
This means two people can have five different relationships, or no relationship. The five relationships are partners, enemies, abuse, targetting, and idolising.
Partners have a stable relationship.
If another person and I have no opinions of each other, we are strangers.
Two people might have the same opinion of each other. If there is mutual love, they are partners. If there is mutual hate, they are enemies.
Let's say we can describe someone's opinion of someone else as love or hate. They might also have no opinion.
Things are a bit more complicated when people's opinions of each other differ.
If another person and I have no opinion of each other, we do not have a relationship. We are strangers to each other.
If we love someone, we want to help them. If we hate someone, we wish them harm.
The nature of helping and harming is kept deliberately ambiguous. Harm might be justified, if someone's interests are evil.
There are five types of relationship between two people. Partners love each other. Enemies hate each other. When two people have different opinions of each other, we will say their relationship is tormenting. The tormenting relationships are idolising, targetting, or abusive. If someone loves a person who has no opinion of them, they are their idol.
If two people love each other, they are partners. If they hate each other, they are enemies.
If two people have different opinions of each other, things are more complicated. We will say they have a tormenting relationship.
If I love someone but they have no opinion of me, this is unrequited love. We will call them my idol.
If I hate someone but they have no opinion of me, we will call them my target.
If I love someone but they hate me, we will call them my abuser.
There are three high-level types of relationship. They are partnered, hostile, and tormenting.
Tormenting relationships have three sub-types. They are idolising, targetting, and abusive.
Now we may talk about relationship triangles. If we take three people, their relationships will form a triangle. Relationship triangles have different dynamics.
We are primarily concerned with their stability, the likelihood they will transition into different forms.
For example, if three people love each other, this is analogous to a stable family or polyamorous throuple.
This is one of the few truly stable triangles. We will call it a throuple.
Slightly less stable is the V triad, where one person has two partners but their partners are not themselves in a relationship.
The V triad has several sub-types.
In a functional V triad, someone's partners simply do not have a relationship with each other. This is semi-stable. We say it's semi-stable because if someone's partners form a relationship, the triad could become dysfunctional.
In a dysfunctional V triad, someone's partners have a tormenting relationship or worse, a hostile relationship. This is unstable.
We also find the exposed alliance, where someone's idol is a stranger to their partner. These may be semi-stable, but they carry risk.
They have two forms,  These exist where one partner has an idol, and who is hated by the other. These alliances are in a sense abusive by proxy, as one person seeks to harm the one who their partner loves.
Situations with only one partnership represent alliances. These are rarely entirely stable. These are also the most complex forms of triangle.
An alliance may be strong, exposed, weak, or toxic.
In a strong alliance, two partners have the same opinion of a third party.
If at peace, they have no opinion of the third party. This is only semi-stable, since if a relationship develops the alliance may be threated.
Note also the alliance may have unseen risks. If the partners are hated by the third party, but have no opinions on it themselves, they are in jeopardy. Even worse is if both partners have a shared abuser.
The safety of the alliance can be ambiguous. Notably, if the third party hates one partner but loves the other, there are two possibilities. Ideally, the third party is constrained by their own internal conflict. However they may seek to break the alliance apart instead.
At war, there is a hated third party. The situation is semi-stable, as it involves conflict. The opinions of the third party are irrelevant. They are better off as a common enemy. Worst for them is being abused by both members of the alliance.
In addition to the alliance at war and the alliance at peace, there is a third type of strong alliance. This is the perfect alliance, notably the only one which is truly stable. If both partners have a common idol, their alliance is perfectly stable. This may not be optimal for the partners themselves, as unrequited love creates pain. But it guarantees the stability of the relationship. Still there seems to be a spiritual significance to the perfect alliance. Perhaps it shows the value of faith in a higher power, or commitment to a greater cause.
In the exposed alliance, one partner has an idol. This does not particularly test the other's virtue, but it does test their vice. Jealousy may destroy the alliance. Note here we may produce an interesting insight. Jealousy may not be entirely unfounded. If one partner has an idol, and the other partner is that idol's target, there is danger. The other partner is, in fact, threatened by the idol, although they cannot know it.
