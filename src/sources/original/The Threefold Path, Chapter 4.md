Enzu states the soul comes in three parts. The first is the animal soul, which she had the unfortunate tendency to associate with the prejudices of our time. Wolves, for the Anthari and Bezic peoples. Goats, for the so-called noble Eurithy. And finally river-mint and bats for the Turst and Pingoli.

The animal soul, says Enzu, is genderless and lives in the present. The human soul is not always consistently described. Enzu considered it the trinity's child, looking to its parent for guidance and making demands of the companion animal beneath it. Other scholars, especially the Witnesses, consider its form to be that of an adult. They point out that though the adult stands in mastery over the world they survey, that they have not learned the lessons of that mastery. That only later, gifted with regret, will they understand the fruits of their rule.

Enzu states that the soul comes in three parts, although his contemporaries prefer four, with the latter two loosely conjoined. First, comes the animal soul. It twitches and feels its body responding to the world without.

Some, notably Pion, claim the body itself is a soul.

Each layer is responsible for the one below it. The animal soul, when it's awake, is either content or scared. It fears hunger, violence, solitude, thirst. It possess instincts to survive the world. Incidentally the world is sometimes conceived of as a soul below the animal. Dark and chaotic, it cannot be known. But the animal tends its needs. The owl eats the mouse, the mouse eats the grass, and the grass eats the earth. It is pleasing to the world, that this continue, although it's purpose is unknown to us. As is the yet deeper sould which is tended by the world.

The thinking soul thinks constantly. When the animal cries out, the thinking soul applies its arts and searches for solutions. For this reason it has built civilisation. Monarchs, castles, farmland. But the animal soul may be betrayed. For if the thinking soul fails in its duty, and is not sufficiently wise, then the animal's needs will be unmet. And it will become more fearful, and uncontrollable, because it knows its carer cannot support it.

In a sense, all thoughts are lonely. They apply only to themselves, and can be repeated indefinitely for no reward. One can think what ought or might, and the thinking alone will not bring it into being.

The third soul is the elder soul. It fears death, but cares for its children. It is at once the parent, admonishing its child (the thinking soul). It is also the pilot of a ferry. It sees the best path and avoids the stones in the river.