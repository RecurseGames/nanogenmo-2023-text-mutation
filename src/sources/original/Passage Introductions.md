Cats attack mice. Mice attack cats. Can they be said to be pure? No, it is unlikely.
Consider the three types of cat. Parsimonious, gentle, lewd. Now, the gentle cat will succour the needy. But the lewd cat will dance favourably.
In times past, a lewd cat was said to be whole. Now we know this is not the case.

Where trees sway slowly. One can find a bush within a hole. But the holes have secrets.

I heard this from a wandering cow. It spoke with a voice like fire...

"Hazam idri mbenk, mbenk iluz pastorii"