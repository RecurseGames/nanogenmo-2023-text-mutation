import { Section } from "./frameworks/functions/section";
import nlp from 'compromise';
import { createWorld, getEntityIdentifier, getEntityWeighted } from "./frameworks/simulation";
import { compareImportance, getImportanceWeighted, getOpinionPairImportance, getWorldOpinionPairs, getOpinionTriangles, renderOpinionPair, getImportanceWeightedLocation, getOpinionPairs, getActualOpinion } from "./frameworks/simulation/triangles";
import { Entity } from "./frameworks/simulation/types";
import { getContents } from "./frameworks/simulation/location";
import { createArray } from "./frameworks/utilities/arrays";
import { getRandom, randInt } from "./frameworks/utilities/random";
import { tryGetSuccess } from "./frameworks/utilities/retries";
import { NlpSource } from "./frameworks/nlp/nlpSource";
import { readFile } from "./frameworks/utilities/strings";
import { createFractalNovel } from "./frameworks/fractal/novel";
import { createNlpNovel } from "./frameworks/nlp/nlpNovel";
import { writeFileSync } from "fs";

// const readline = require('readline').createInterface({
//     input: process.stdin,
//     output: process.stdout,
// });


/**
 * cats
  'PresentTense,Infinitive': [ 'love', 'know', 'think', 'speak' ],
  'PresentTense,Infinitive,Imperative': [ 'know', 'wonder', 'sure', 'consider' ],
  'Infinitive': [ 'defend' ],
  'PhrasalVerb,PresentTense,Infinitive': [ 'reach', 'point' ],
  'Infinitive,PresentTense': [ 'intersect' ],
  'PresentTense,Infinitive,Copula': [ 'be' ]

  cats (aux)
  'Auxiliary': [ "don't", 'have', 'has', 'do' ],

cat
  'PresentTense': [ 'makes', 'feels', 'harms', 'requires' ],
  'PresentTense,PhrasalVerb': [ 'comes', 'cries' ],
  'PresentTense,Verb': [ 'twitches' ],

  cat/cats/is
  'PastTense,Passive': [ 'told', 'raised', 'prejudiced', 'meant' ],
  'PastTense': [ 'experienced', 'received', 'intended', 'left' ],
  'PastTense,Passive': [ 'tended' ],
  'PresentTense,Gerund': [ 'saying', 'Talking', 'concerning', 'thinking' ],
  'PresentTense,Gerund,PhrasalVerb': [ 'Lying', 'looking' ],
  'Gerund,PresentTense': [ 'pleasing', 'admonishing' ],

  cat/cats
  'Modal,Auxiliary': [ 'can', 'would', 'will', 'could' ],
  'Modal': [ 'should', 'could', 'would', 'will' ],
 * 
    NEVER
  'Copula,PresentTense': [ 'are', 'is', "isn't", 'am' ],
  'Passive,Auxiliary': [ '', 'been' ],
  'Copula,Auxiliary': [ 'are', 'were', 'is' ],
  'Copula,Passive,Auxiliary': [ 'am', 'is', 'was', 'were' ],
  'Copula,PastTense': [ 'was', 'were' ],
  
  is (aux)
  'PastTense,Participle': [ 'been', 'written', 'given', 'seen' ],
  'PhrasalVerb,Particle': [ 'out', 'in', 'up' ],
  
 */


console.log("Generating novel...");
const novel = createNlpNovel();
console.log("Saving novel...");
writeFileSync("output.html", novel);
console.log("Success.");

// console.log(createFractalNovel());

// while (true) {
//     const s = myNlp.getSentence();

//     console.log(s);
// }

// const world = createWorld();

// console.log("Random entity sampling:\n");
// for (let i = 0; i < 30; i++) {
//     console.log(getEntityIdentifier(getEntityWeighted(world, "character")));
// }

// console.log("Opinions:\n");
// const pairs = getWorldOpinionPairs(world).sort((a, b) => compareImportance(
//     getOpinionPairImportance(b),
//     getOpinionPairImportance(a),
// ));
// for (const pair of pairs) {
//     console.log(renderOpinionPair(pair));
// }

// console.log("\nOpinion trianges:\n");
// const triangles = getOpinionTriangles(world);
// for (const triangle of triangles) {
//     console.log([
//         renderOpinionPair(triangle[0]),
//         renderOpinionPair(triangle[1]),
//         renderOpinionPair(triangle[2]),
//     ].join(" "));
// }


// console.log("\nNow we're gonna loop and look at some important locations...\n");
// let previousLocation: Entity | undefined;
// for (let i = 0; i < 10; i++) {
//     const location = getImportanceWeightedLocation(world);
//     if (location !== previousLocation) {
//         previousLocation = location;
//         console.log(`\nWe turn our attention to ${getEntityIdentifier(location)}...\n`);
//     }

//     const contents = getContents(world, location);
//     const pairs = getOpinionPairs(contents);
//     const events = createArray(Math.abs(7 - randInt(1, 6) - randInt(1, 6) + randInt(2, 4)), () => {
//         return getSuccess(1000, () => {
//             const subject = getRandom(contents);
//             const target = getRandom(contents.filter(e => e != subject));
//             const eventFactories: (() => string | undefined)[] = [
//                 () => {
//                     return "Time passes";
//                 },
//                 () => {
//                     if (getActualOpinion(subject, target) !== "hate") {
//                         return `${getEntityIdentifier(subject)} speaks with ${getEntityIdentifier(target)}`;
//                     }
//                 },
//                 () => {
//                     if (getActualOpinion(subject, target) === "hate") {
//                         return `${getEntityIdentifier(subject)} attacks ${getEntityIdentifier(target)}`;
//                     }
//                 },
//                 () => {
//                     if (getActualOpinion(subject, target) === "love") {
//                         return `${getEntityIdentifier(subject)} helps ${getEntityIdentifier(target)}`;
//                     }
//                 },
//             ];
//             return getRandom(eventFactories)();
//         });
//     });

//     for (const event of events) {
//         console.log(event);
//     }
// }
// // const KAIDARA = nlp(readFile("src/kaidara.txt"));

// const sections: Section[] = [
//     {
//         context: {},
//         output: "",
//     }, {
//         context: {},
//         output: "",
//     },
// ];

// // console.log(KAIDARA.sentences().toPastTense().random().text());
// // console.log(KAIDARA.sentences().toPresentTense().random().text());
// // console.log(KAIDARA.sentences().toFutureTense().random().text());

// const sentence = nlp(
//     // KAIDARA.sentences().random().text()
//     "Hammadi saw others like little grains dazzling wonderfully like a magic jewel imbued with a mystic force, striking sparks between earth and sky."
// );

// // console.log('==== text ====');
// // console.log(sentence.text());
// // console.log('==== sentences ====');
// // console.log(sentence.sentences().json());
// // console.log('==== clauses ====');
// // console.log(sentence.clauses().json());
// // console.log('==== chunks ====');
// // console.log(sentence.chunks().json());
// // for (const chunk of sentence.chunks().json()) {
// //     console.log(chunk);
// // }
// // console.log(JSON.stringify(sentence.json()));
// // console.log(sentence.json()[0]["sentence"]["grammar"]);
// // console.log(sentence.termList());
