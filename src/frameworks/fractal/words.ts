import { getRandom } from "../utilities/random";
import { ELEMENTS, FractalElement, getElementLookup } from "./elements";

export function getWords(...sources: string[]): string[] {
    const words: string[] = [];
    for (let source of sources) {
        words.push(...source.split(","));
    }
    return words;
}

const ADJECTIVES = getElementLookup(
    getWords("short,brisk,quick,fearful,shrinking,cold,still,jealous,thin,narrow,static,lesser"),
    getWords("hidden,secret,worn,quiet,dusky,anxious,seeking,wet,peaceful,wary,arrested,aged,wise"),
    getWords("slow,long,hard,meandering,halting,tired,calm,growing,dry,sharp,weary,golden,indirect"),
    getWords("dangerous,spiky,risky,long,sudden,unknown,violent,metallic,hot,angry,toxic,greater"),
);

export function getAdjective(element: FractalElement): string {
    return getRandom(ADJECTIVES[element || getRandom(ELEMENTS)]);
}

export function getNumberWord(value: number): string {
    const numbersBelow20 = [
        'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine',
        'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen',
        'eighteen', 'nineteen'
    ];

    if (value < 20) {
        return numbersBelow20[value];
    } else if (value < 100) {
        const prefix = [
            '', '', 'twenty-', 'thirty-', 'forty-', 'fifty-', 'sixty-', 'seventy-', 'eighty-', 'ninety-'
        ][Math.floor(value / 10)];
        const remainder = value % 10;
        return `${prefix}${remainder ? '-' + numbersBelow20[remainder] : ''}`;
    } else if (value < 1000) {
        const hundreds = Math.floor(value / 100);
        const remainder = value % 100;
        return `${numbersBelow20[hundreds]} hundred${remainder ? ' and ' + getNumberWord(remainder) : ''}`;
    } else {
        throw new Error("Number too big");
    }
}


export function getPlural(word: string) {
    if (word.endsWith("y")) {
        return word.substring(0, word.length - 1) + "ies";
    } else if (word.endsWith("s")) {
        return `${word}es`;
    } else {
        return `${word}s`;
    }
}
