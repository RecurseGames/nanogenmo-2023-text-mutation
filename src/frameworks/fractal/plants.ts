import { getRandom } from "../utilities/random";
import { getSuccess } from "../utilities/retries";
import { getCorpus } from "./integrations";

const PLANT_NAMES = getCorpus("plants");
const PLANT_PREFIX_SET = new Set<string>();
const PLANT_SUFFIX_SET = new Set<string>();
for (let plantName of PLANT_NAMES) {
    const parts = plantName.split(" ");
    PLANT_SUFFIX_SET.add(parts.pop()!);
    PLANT_PREFIX_SET.add(parts.join(" "));
}

const PLANT_PREFIXES = [...PLANT_PREFIX_SET];
const PLANT_SUFFIXES = [...PLANT_SUFFIX_SET];

const UNIQUE_PLANTS = new Set<string>();

export function createUniquePlant(): string {
    return getSuccess(1000, () => {
        const plant = createPlant();
        if (!UNIQUE_PLANTS.has(plant)) {
            UNIQUE_PLANTS.add(plant);
            return plant;
        }
    });
}

export function createPlant(): string {
    return `${getRandom(PLANT_PREFIXES)} ${getRandom(PLANT_SUFFIXES)}`;
}
