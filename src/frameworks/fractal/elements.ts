export type FractalElement = "COLD_DRY" | "COLD_WET" | "HOT_DRY" | "HOT_WET";

export const ELEMENTS: FractalElement[] = ["COLD_DRY", "COLD_WET", "HOT_DRY", "HOT_WET"];

export function getElementLookup<T>(
    COLD_DRY: T[],
    COLD_WET: T[],
    HOT_DRY: T[],
    HOT_WET: T[],
): Record<FractalElement, T[]> {
    return {
        COLD_DRY,
        COLD_WET,
        HOT_DRY,
        HOT_WET,
    };
};
