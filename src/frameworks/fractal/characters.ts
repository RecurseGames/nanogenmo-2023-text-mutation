import { getRandom } from "../utilities/random";

export function createFractalNamePart(): string {
    return `${getRandom(
        "nt,n,k,kr,sh,sht,s,ss,sl,sr,lb,rp,rt,rb,r,ld".split(",")
    )}${getRandom([
        "a", "u", "e", "i", "u", "a", "a", "i", "i"
    ])}`
}

export function createFractalNameEnd(): string {
    return getRandom("t,p,s,f,g,l,z,b,n,m,st,rk,ns,ks".split(","));
}

export function createFractalNameStart(): string {
    return `${getRandom([
        "B", "P", "Sh", "K", "Ng", "Pl", "R", "Ts", "R", "T", "D", "G", "N",
    ])}${getRandom([
        "a", "u", "e", "i", "u", "a", "a", "i", "i"
    ])}`
}

export function createFractalName(): string {
    return getRandom([
        () => `${createFractalNameStart()}${createFractalNamePart()}`,
        () => `${createFractalNameStart()}${createFractalNamePart()}${createFractalNamePart()}`,
        () => `${createFractalNameStart()}${createFractalNamePart()}${createFractalNameEnd()}`,
        () => `${createFractalNameStart()}${createFractalNamePart()}${createFractalNamePart()}${createFractalNameEnd()}`,
    ])();
}
