import { readFileSync } from "fs";
import { getRandom } from "../utilities/random";

const CORPUSES: Record<string, string[]> = {};

export function getCorpus(filename: string): string[] {
    return CORPUSES[filename] ??= readFileSync(`src/sources/attributed/${filename}.txt`, "utf-8")
        .split("\n")
        .map(line => line.trim())
        .filter(line => line && !line.startsWith("http"));
}

export function getCorpusLine(filename: string): string {
    return getRandom(getCorpus(filename));
}
