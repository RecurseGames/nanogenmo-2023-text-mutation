import { getRandom } from "../utilities/random";

export function createFractalDirection(): string {
    return getRandom(["North", "South", "East", "West"]);
}

export function createFractalDirectionAdjective(): string {
    const adjectives = "slow,long,hard,meandering,short,brisk,quick,hidden,secret,worn,quiet,dusky,dangerous,spiky,risky,long"
    return getRandom("f".split(","));
}

export function createFractalLocation(): string {
    return getRandom([
        () => ``,
    ])();
}

export function createFractalLocationPlural(): string {
    return getRandom("hills,steppes,swamps,forests,lakes,caves,".split(","));
}

export function createFractalRelativeLocation(): string {
    return getRandom([
        () => getRandom(["above", "below", "beneath", "near"]),
        () => `a ${createFractalDirectionAdjective()} ${getRandom(["walk", "trip", "journey"])} from`,
        () => `${createFractalDirection()} of`,
    ])();
}

