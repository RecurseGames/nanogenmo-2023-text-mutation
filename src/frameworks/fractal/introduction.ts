import { getSystemErrorMap } from "util";
import { createArray } from "../utilities/arrays";
import { getChoices, getRandom, randInt } from "../utilities/random";
import { tryGetSuccess } from "../utilities/retries";
import { ELEMENTS } from "./elements";
import { getAdjective, getNumberWord, getPlural } from "./words";

export function createFractalIntroduction(): string {
    return "";
}

export function createFractalIntroductionStart(): string {


    /**
     * There is a forest, DESCRIPTOR. It is a CLASS. X can be found there, but not Y.
     * If you X, Y.
     * There are X kinds of Y. AY, BY, and CY. CY is the most D. BY is more P than CY. A BY is much like a W. It U and I.
     * 
     * Y says L.
     * 
     * I drew a herb. It symbolises I.
     */

    return getRandom([
        () => ``,
    ])();
}

export function createFractalSchema(): string {
    const count = randInt(3, 5);
    const noun = "anxiety";
    const elements = createArray(count, () => getRandom(ELEMENTS));
    const adjectives = tryGetSuccess(100, () => {
        const result = elements.map(getAdjective);
        if (result.length == new Set(result).size) {
            return result;
        }
    });

    const categoryCount = getRandom([
        `${getNumberWord(count)} ${getPlural(noun)}`,
        `${getNumberWord(count)} types of ${noun}`
    ]);

    const intro = getRandom([
        () => `There are ${categoryCount}`,
        () => `We will consider ${categoryCount}`,
        () => `${categoryCount} exist`,
        () => `We can find ${categoryCount}`,
    ])();

    return (
        `There are ${getNumberWord(count)} typ`
    );
}