export function getRandom<Element>(elements: Element[]): Element {
    if (elements.length) {
        return elements[Math.floor(Math.random() * elements.length)];
    } else {
        throw new Error("Cannot get random element from empty array");
    }
}

export function randInt(min: number, max: number): number {
    return min + Math.floor(Math.random() * (1 + max - min));
}

export function getChoices<T>(items: T[], count: number): T[] | undefined {
    if (items.length >= count) {
        const result: T[] = [];
        items = items.slice();
        while (result.length < count) {
            const i = Math.floor(Math.random() * items.length);
            result.push(items[i]);
            items[i] = items[items.length - 1];
            items.length--;
        }
        return result;
    }
}
