import nlp from "compromise/three";
import { readFileSync, writeFileSync } from "fs";
import { NlpSource } from "../nlp/nlpSource";

export function readFile(path: string): string {
    return readFileSync(path, "utf-8");
}

export function createHash(length: number = 6): string {
    if (length >= 0) {
        return Math.random().toString(18).substring(2, length + 2).padStart(length, "0");
    } else {
        throw new Error("Invalid length.");
    }
}

export function levenshteinDistance(value1: string, value2: string): number {
    const matrix: number[][] = [];
    for (let i = 0; i <= value1.length; i++) {
        matrix[i] = [];
        matrix[i][0] = i;
    }
    for (let j = 0; j <= value2.length; j++) {
        matrix[0][j] = j;
    }

    for (let i = 1; i <= value1.length; i++) {
        for (let j = 1; j <= value2.length; j++) {
            const cost = value1.charAt(i - 1) === value2.charAt(j - 1) ? 0 : 1;
            matrix[i][j] = Math.min(
                matrix[i - 1][j] + 1,
                matrix[i][j - 1] + 1,
                matrix[i - 1][j - 1] + cost
            );
        }
    }

    return matrix[value1.length][value2.length];
}

export function escapeHtml(text: string): string {
    var encodedCharacters: Record<string, string> = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, match => encodedCharacters[match]);
}

export function getWordCount(text: string): number {
    nlp("").people
    return nlp(text).terms().length;
}

export function ensureTerminator(text: string, terminator: string): string {
    return text.endsWith(terminator) ? text : `${text}${terminator}`;
}
