export function createArray<T>(length: number, createItem: () => T) {
    const array: T[] = [];
    for (let i = 0; i < length; i++) {
        array.push(createItem());
    }
    return array;
}

export function createRange(length: number) : number[] {
    const range: number[] = [];
    for (let i = 0; i < length; i++) {
        range.push(i);
    }
    return range;
}
