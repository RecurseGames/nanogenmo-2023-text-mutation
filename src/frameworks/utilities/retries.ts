export function tryGetSuccess<T>(attempts: number, tryGetSuccess: () => T): T {
    if (!Number.isSafeInteger(attempts)) {
        throw new Error("Must make a safe number of attempts.");
    } else if (attempts < 1) {
        throw new Error("Must make at least one attempt.");
    }
    let result: T;
    do {
        result = tryGetSuccess();
    } while (!result && --attempts > 0);
    return result;
}

export function getSuccess<T>(attempts: number, _tryGetSuccess: () => T): NonNullable<T> {
    const success = tryGetSuccess(attempts, _tryGetSuccess);
    if (!success) {
        throw new Error("Failed to get successful value.");
    }
    return success;
}
