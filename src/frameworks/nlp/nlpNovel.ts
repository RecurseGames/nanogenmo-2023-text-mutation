import { NlpSource } from "./nlpSource";
import { createArray } from "../utilities/arrays";
import { getRandom, randInt } from "../utilities/random";
import { getSuccess } from "../utilities/retries";
import { escapeHtml, getWordCount, readFile } from "../utilities/strings";
import { NlpGenerator } from "./nlpGenerator";
import { getCorpus } from "../fractal/integrations";
import { createUniquePlant } from "../fractal/plants";
import { createFractalName } from "../fractal/characters";

function createBook(title: string, body: string): string {
    return `<!DOCTYPE html>
<html lang="en">
    <title>${escapeHtml(title)}</title>
    <style>
        @font-face {
            font-family: 'Essays 1743';
            src: url('Essays1743.ttf') format('truetype');
        }
        @font-face {
            font-family: 'Linux Libertine';
            src: url('LinLibertine_R.ttf') format('truetype');
        }
        html {
            font-family: 'Linux Libertine', Helvetica, sans-serif;
            font-size: 1.2rem;
        }
        h1 {
            font-size: 4rem;
        }
        h2 {
            font-size: 2rem;
        }
        h3 {
            font-size: 1.5rem;
        }
        h4 {
            font-weight: bold;
        }
        header {
            text-align: center;
        }
        h1, h2, h3 {
            font-family: 'Essays 1743', Georgia, serif;
        }
        h1, h2, h3, h4, p {
            margin: 0 0 0.5rem 0;
        }
        header, article, .page {
            padding: 1rem;
            page-break-after: always;
        }
        .page {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }
        .poem {
            font-style: italic;
            text-align: center;
        }
        .divider {
            height: 2rem;
        }
    </style>
${body}`;
}

function createElement(tag: string, ...html: string[]): string {
    return `<${tag}>${html.join("")}</${tag}>`;
}

function createTextElement(tag: string, content: string): string {
    return createElement(tag, escapeHtml(content));
}

function createParagraph(generator: NlpGenerator, metrics: Metrics): string {
    console.log("Generating paragraph...");
    const content = generator.getRandomParagraph();
    if (metrics) {
        metrics.wordCount += getWordCount(content);
    }
    return createTextElement("p", content);
}

function createPoemHtml(generator: NlpGenerator, metrics: Metrics, sentenceCollector?: string[]): string {
    return "<p class=\"poem\">" + createArray(randInt(3, 8), () => {
        const line = generator.getRandomSentence();
        if (sentenceCollector) {
            sentenceCollector.push(line);
        }
        if (metrics) {
            metrics.wordCount += getWordCount(line);
        }
        return escapeHtml(line);
    }).join("<br></br>") + "</p>";
}

function createTitle(generator: NlpGenerator): string {
    let maxLength = 25;
    console.log("Generating title...");
    const title = getSuccess(1000, () => {
        const result = generator.getRandomSentence();
        if (result.length < maxLength) {
            return result;
        } else {
            maxLength += 0.25;
        }
    });
    return title;
}

function createSection(...html: string[]): string {
    return `<article>${html.join("")}</article>\n\n`;
}

function createSpecialPage(html?: string): string {
    return `<div class="page">${html || ""}</div>`;
}

function createChapter(generator: NlpGenerator, bodyGenerator: NlpGenerator | null, metrics: Metrics): Chapter {
    return {
        title: createTitle(generator),
        bodyHtml: createChapterBody(bodyGenerator || generator, metrics),
    };
}

function createChapterBody(generator: NlpGenerator, metrics: Metrics): string {
    return createArray(randInt(4, 15), () => createParagraph(generator, metrics)).join("\n");
}

interface Metrics {
    wordCount: number;
}

interface Book {
    title: string;
    headerHtml?: string;
    chapters?: Chapter[];
}

interface Chapter {
    title: string;
    bodyHtml: string;
}

function createSource(path: string): NlpSource {
    console.log(`Loading ${path}...`);
    const text = readFile(path);
    return new NlpSource(text);
}

function createDescentChapters(generator: NlpGenerator, metrics: Metrics, chapterComplexities: number[]): Chapter[] {
    const complexity = [...chapterComplexities].sort()[chapterComplexities.length - 1];
    const sources = createArray(complexity, () => getSuccess(1000, () => {
        const sentence = generator.getRandomSentence();
        if (sentence.length < 80) {
            return sentence;
        }
    }));
    const chapters: Chapter[] = [];
    for (let chapterComplexity of chapterComplexities) {
        const chapterSources = sources.slice(0, chapterComplexity);
        const chapterGenerator = new NlpGenerator([new NlpSource(chapterSources.join("\n"))]);
        const chapter = createChapter(chapterGenerator, null, metrics);
        chapters.push(chapter);
    }
    return chapters;
}

export function createNlpNovel() {
    const TRIANGLES_CHAPTER_1_PATH = "src/sources/original/On Triangles, Chapter 1.md";
    const TRIANGLES = createSource(TRIANGLES_CHAPTER_1_PATH);
    const THREEFOLD_INTRO = createSource("src/sources/original/The Threefold Path, Introduction.md");
    const THREEFOLD = createSource("src/sources/original/The Threefold Path, Chapter 4.md");
    const MUSINGS = createSource("src/sources/original/Musings.md");
    const WATCHFUL = createSource("src/sources/original/Watchful Skies.md");
    const PRISM = createSource("src/sources/original/Prism, Chapter 2.md");
    const LOVE = createSource("src/sources/original/Love Letter.md");
    const SCHOOL_CONTENTS = createSource("src/sources/public_domain/An Italian Schoolboy's Journal, Contents.txt");
    const SCHOOL = createSource("src/sources/public_domain/An Italian Schoolboy's Journal.txt");
    const REVELATIONS = createSource("src/sources/public_domain/revelations.txt");
    const SONG_SOLOMON = createSource("src/sources/public_domain/solomon.txt");
    const NOBODY = createSource("src/sources/public_domain/The Diary of a Nobody.txt");
    const FLOWER_INDICES = createSource("src/sources/public_domain/THE FLORAL SYMBOLISM OF THE GREAT MASTERS, Indices.txt");
    const FLOWER = createSource("src/sources/public_domain/THE FLORAL SYMBOLISM OF THE GREAT MASTERS.txt");
    const MAN_INDICES = createSource("src/sources/public_domain/The Journal of a Disappointed Man, Indices.txt");
    const MAN_INTRO = createSource("src/sources/public_domain/The Journal of a Disappointed Man, Introduction.txt");
    const MAN = createSource("src/sources/public_domain/The Journal of a Disappointed Man.txt");
    const HISTORY_BOOKS = createSource("src/sources/public_domain/The Outline of History, Books.txt");
    const HISTORY_CONTENTS = createSource("src/sources/public_domain/The Outline of History, Contents.txt");
    const HISTORY_INTRO = createSource("src/sources/public_domain/The Outline of History, Introduction.txt");
    
    const HERB_AUTHOR = createSource("src/sources/public_domain/The Complete Herbal/Sections/Author.txt");
    const HERB_EPISTLE = createSource("src/sources/public_domain/The Complete Herbal/Sections/Epistle.txt");
    const HERB_Directions = createSource("src/sources/public_domain/The Complete Herbal/Sections/Directions.txt");
    const HERB_Commentary = createSource("src/sources/public_domain/The Complete Herbal/Sections/Commentary.txt");
    const HERB_VIRTUES = createSource("src/sources/public_domain/The Complete Herbal/Sections/Virtues.txt");
    const HERB_DESCRIPTIONS = createSource("src/sources/public_domain/The Complete Herbal/Descriptions.txt");
    const HERB_TIMES = createSource("src/sources/public_domain/The Complete Herbal/Times.txt");
    const HERB_PLACES = createSource("src/sources/public_domain/The Complete Herbal/Places.txt");
    const HERB_GOVERNMENTS = createSource("src/sources/public_domain/The Complete Herbal/Governments.txt");
    const HERB_PREAMBLES = createSource("src/sources/public_domain/The Complete Herbal/Preambles.txt");
    
    const OCCULT_CONTENT = createSource("src/sources/public_domain/Hidden Symbolism of Alchemy and the Occult Arts/Content.txt");
    const OCCULT_INDEX = createSource("src/sources/public_domain/Hidden Symbolism of Alchemy and the Occult Arts/Index.txt");
    const OCCULT_PREFACE = createSource("src/sources/public_domain/Hidden Symbolism of Alchemy and the Occult Arts/Preface.txt");

    const POEMS_CONTENT = createSource("src/sources/public_domain/The Flowers of Evil/Content.txt");
    const POEMS_TITLES = createSource("src/sources/public_domain/The Flowers of Evil/Titles.txt");
    
    const RECIPES = createSource("src/sources/public_domain/Recipes.txt");
    const RECIPES_TITLES = createSource("src/sources/public_domain/Recipes, Titles.txt");

    const TITLE_SOURCES = [
        HISTORY_BOOKS, MAN_INDICES, FLOWER_INDICES, RECIPES_TITLES,
        SCHOOL_CONTENTS, HISTORY_CONTENTS,
        POEMS_TITLES, OCCULT_INDEX,
    ];

    const metrics: Metrics = {
        wordCount: 0,
    };

    console.log("Generating title...");
    const titleSource = new NlpGenerator([WATCHFUL, THREEFOLD, PRISM]);
    const title = createTitle(titleSource);

    console.log("Generating author...");
    const firstNames = new Set<string>();
    const lastNames = new Set<string>();
    const honorifics = new Set<string>();
    for (let generator of [
        REVELATIONS, NOBODY, FLOWER_INDICES, FLOWER,
        MAN_INDICES, MAN_INTRO, MAN,
    ]) {
        generator.getFirstNames().forEach(e => firstNames.add(e));
        generator.getLastNames().forEach(e => lastNames.add(e));
        generator.getHonorifics().forEach(e => honorifics.add(e));
    }
    const author = `${getRandom([...honorifics])} ${getRandom([...firstNames])} ${getRandom([...lastNames])}`

    // Render cover page and opposite
    let novel = createElement("header",
        createTextElement("h1", title),
        createTextElement("p", author),
    );
    novel += createSpecialPage();

    // Render dedication and opposite
    const dedicationSource = new NlpGenerator([SONG_SOLOMON, LOVE]);
    novel += createSpecialPage(createPoemHtml(dedicationSource, metrics));
    novel += createSpecialPage();

    // Glossary data
    const GLOSSARY_TERMS = createArray(48, () => {
        return createFractalName();
    }).sort();
    const WORD_DEFINITION_GENERATOR = new NlpGenerator(
        [new NlpSource(getCorpus("landforms").join("\n") + getCorpus("rocks").join("\n"))],
        [MAN_INTRO, HERB_VIRTUES, SONG_SOLOMON, REVELATIONS],
    );

    // Create books
    const books: Book[] = [{
        title: "Foreword",
        headerHtml: readFile("src/sources/original/foreword.html"),
    }, {
        title: "Preface",
        headerHtml: createChapterBody(
            new NlpGenerator([MUSINGS, THREEFOLD_INTRO]), 
            metrics,
        ),
    }, {
        title: "The Ladder",
        chapters: [
            createChapter(new NlpGenerator([MUSINGS, HISTORY_INTRO, THREEFOLD_INTRO]), null, metrics),
            createChapter(new NlpGenerator([HERB_EPISTLE, MAN_INTRO, HISTORY_INTRO]), null, metrics),
            createChapter(new NlpGenerator([HERB_EPISTLE, OCCULT_PREFACE, THREEFOLD_INTRO]), null, metrics),
            createChapter(new NlpGenerator([TRIANGLES]), null, metrics),
            createChapter(new NlpGenerator([TRIANGLES, OCCULT_CONTENT]), null, metrics),
            createChapter(new NlpGenerator([OCCULT_CONTENT]), null, metrics),
            createChapter(new NlpGenerator([...TITLE_SOURCES, OCCULT_CONTENT]), null, metrics),
            createChapter(new NlpGenerator([OCCULT_CONTENT]), new NlpGenerator([RECIPES, HERB_Commentary]), metrics),
            createChapter(new NlpGenerator([OCCULT_CONTENT]), new NlpGenerator([...TITLE_SOURCES, HERB_Commentary]), metrics),
            createChapter(new NlpGenerator([OCCULT_CONTENT]), new NlpGenerator([MAN, SCHOOL]), metrics),
            createChapter(new NlpGenerator([HERB_VIRTUES, TRIANGLES]), null, metrics),
            createChapter(new NlpGenerator([TRIANGLES]), new NlpGenerator(TITLE_SOURCES), metrics),
            createChapter(new NlpGenerator([MUSINGS, HISTORY_INTRO, THREEFOLD_INTRO, PRISM, WATCHFUL]), null, metrics),
            createChapter(new NlpGenerator([PRISM, WATCHFUL]), null, metrics),
            createChapter(new NlpGenerator([PRISM, WATCHFUL, REVELATIONS]), null, metrics),
            createChapter(new NlpGenerator([REVELATIONS]), null, metrics),
            createChapter(new NlpGenerator([REVELATIONS, SONG_SOLOMON]), null, metrics),
            createChapter(new NlpGenerator([SONG_SOLOMON]), null, metrics),
            createChapter(new NlpGenerator([SONG_SOLOMON, MAN]), null, metrics),
            createChapter(new NlpGenerator([MAN]), null, metrics),
            createChapter(new NlpGenerator([MAN, SCHOOL]), null, metrics),
            createChapter(new NlpGenerator([SCHOOL]), null, metrics),
            createChapter(new NlpGenerator([SCHOOL, MAN_INTRO, THREEFOLD]), null, metrics),
            createChapter(new NlpGenerator([MAN_INTRO, THREEFOLD]), null, metrics),
            createChapter(new NlpGenerator([MAN_INTRO, THREEFOLD, LOVE]), null, metrics),
            createChapter(new NlpGenerator([HERB_AUTHOR, LOVE]), null, metrics),
            createChapter(new NlpGenerator([HERB_AUTHOR, REVELATIONS]), null, metrics),
            createChapter(new NlpGenerator([HERB_AUTHOR, NOBODY]), null, metrics),
            createChapter(new NlpGenerator([FLOWER]), null, metrics),
            createChapter(new NlpGenerator([POEMS_CONTENT]), null, metrics),
            createChapter(new NlpGenerator([POEMS_CONTENT]), new NlpGenerator([RECIPES, OCCULT_CONTENT]), metrics),
            createChapter(new NlpGenerator([HISTORY_CONTENTS]), null, metrics),
            createChapter(new NlpGenerator([HERB_VIRTUES, POEMS_CONTENT]), null, metrics),
        ],
    }, {
        title: "The Descent", // About 3000 words
        chapters: createDescentChapters(
            new NlpGenerator(
                [REVELATIONS, MUSINGS, MAN_INTRO],
                [REVELATIONS, MUSINGS, MAN_INTRO, FLOWER_INDICES],
            ),
            metrics,
            [256, 64, 16, 4, 1],
        ),
    },
    {
        title: "Bibliography",
        headerHtml: readFile("src/sources/original/bibliography.html"),
    },
    {
        title: "Appendices",
        chapters: [{
            title: "Appendix I: Recipes",
            bodyHtml: createArray(24, () => {
                const title = new NlpGenerator([RECIPES_TITLES], [RECIPES_TITLES, FLOWER_INDICES]);
                const recipe = new NlpGenerator([RECIPES, HERB_Commentary, HERB_Directions]);
                return [
                    createTextElement("h4", title.getRandomSentence()),
                    createTextElement("p", recipe.getRandomParagraph()),
                ].join("\n");
            }).join("\n"),
        }, {
            title: "Appendix II: Plants",
            bodyHtml: createArray(36, () => {
                const plant = createUniquePlant();
                return [
                    `<h4>${escapeHtml(plant)}</h4>`,
                    (randInt(1, 6) >= 5 ? createParagraph(new NlpGenerator([
                        HERB_DESCRIPTIONS, HERB_PREAMBLES,
                    ]), metrics) : ""),
                    `<p>TIME - ${escapeHtml(new NlpGenerator([HERB_TIMES]).getRandomSentence())}</p>`,
                    `<p>PLACE - ${escapeHtml(new NlpGenerator([HERB_PLACES]).getRandomSentence())}</p>`,
                    `<p>VIRTUES - ${escapeHtml(new NlpGenerator([HERB_GOVERNMENTS]).getRandomParagraph())}</p>`,
                ].join("\n");
            }).join("\n"),
        }, {
            title: "Appendix III: Glossary",
            bodyHtml: GLOSSARY_TERMS.map(word => {
                return createTextElement("p", `${word.toLocaleUpperCase()} - ${WORD_DEFINITION_GENERATOR.getRandomSentence()}`);
            }).join("\n"),
        }, {
            title: "Appendix IV: Poems",
            bodyHtml: [
                new NlpGenerator([SONG_SOLOMON, LOVE, POEMS_CONTENT], [THREEFOLD_INTRO, THREEFOLD]),
                new NlpGenerator([SONG_SOLOMON, LOVE, POEMS_CONTENT], [REVELATIONS]),
                new NlpGenerator([SONG_SOLOMON, LOVE, POEMS_CONTENT], [REVELATIONS, WATCHFUL]),
                new NlpGenerator([SONG_SOLOMON, LOVE, POEMS_CONTENT], [WATCHFUL]),
                new NlpGenerator([SONG_SOLOMON, LOVE, POEMS_CONTENT], [HERB_GOVERNMENTS, HERB_VIRTUES]),
                new NlpGenerator([SONG_SOLOMON, LOVE, HERB_GOVERNMENTS, HERB_VIRTUES], [HERB_GOVERNMENTS, HERB_VIRTUES]),
                new NlpGenerator([WATCHFUL]),
                new NlpGenerator([WATCHFUL, THREEFOLD]),
                new NlpGenerator([THREEFOLD]),
                new NlpGenerator([WATCHFUL, SONG_SOLOMON]),
                new NlpGenerator([WATCHFUL, SONG_SOLOMON, RECIPES_TITLES]),
                new NlpGenerator(TITLE_SOURCES),
                new NlpGenerator(TITLE_SOURCES, [SONG_SOLOMON, LOVE, HISTORY_INTRO]),
                new NlpGenerator([POEMS_CONTENT], [WATCHFUL, LOVE]),
                new NlpGenerator([POEMS_CONTENT], [MAN_INTRO, OCCULT_PREFACE]),
                new NlpGenerator([POEMS_CONTENT], [OCCULT_CONTENT]),
                new NlpGenerator([POEMS_CONTENT], [HISTORY_CONTENTS]),
                new NlpGenerator([POEMS_CONTENT], [FLOWER]),
                new NlpGenerator([POEMS_CONTENT], [RECIPES, FLOWER, ...TITLE_SOURCES]),
                new NlpGenerator([POEMS_CONTENT], [SONG_SOLOMON, LOVE]),
            ].map(e => {
                const sentences: string[] = [];
                const html = createPoemHtml(e, metrics, sentences);
                const title = new NlpGenerator(TITLE_SOURCES, [new NlpSource(sentences.join("\n"))]);
                return `<h4>${escapeHtml(createTitle(title))}</h4>${html}`;
            }).join("<div class=\"divider\"></div>"),
        }, {
            title: "Appendix V: References",
            bodyHtml: createArray(24, () => {
                const title = new NlpGenerator(TITLE_SOURCES);
                const contentGenerator = new NlpGenerator([HERB_AUTHOR, MAN_INTRO, HERB_Commentary]);
                return [
                    createTextElement("h4", title.getRandomSentence()),
                    createTextElement("p", contentGenerator.getRandomParagraph()),
                ].join("\n");
            }).join("\n"),
        }, {
            title: "Appendix VI: On Triangles",
            bodyHtml: [
                createTextElement("p", "The following is an excerpt from Chapter 1 of the text On Triangles."),
                ...readFile(TRIANGLES_CHAPTER_1_PATH).split("\n").filter(e => e.trim()).map(e => `<p class="poem">${escapeHtml(e)}</p>`)
            ].join("\n"),
        }, {
            title: "Appendix VII: NaNoGenMo Diary",
            bodyHtml: readFile("src/sources/original/diary.html"),
        }],
    }];

    // Create contents
    let contentsBodyHtml = "";
    const booksHtml = books.map((book, bookIndex) => {
        const bookId = `book-${bookIndex}`;
        contentsBodyHtml += `<p><a href="#${bookId}"><strong>${escapeHtml(book.title)}</strong></a></p>`;
        const bookHeading = `<h2 id="${bookId}">${escapeHtml(book.title)}</h2>`;
        let bookHtml = book.headerHtml ? `<article>${bookHeading}${book.headerHtml}</article>` : createSpecialPage(bookHeading);

        if (book.chapters) {
            let bookContentList = "";
            bookHtml += book.chapters.map((chapter, chapterIndex) => {
                const chapterId = `book-${bookIndex}-chapter-${chapterIndex}`;
                bookContentList += `<li><a href="#${chapterId}">${escapeHtml(chapter.title)}</a></li>`;
                return (
                    `<article id="${chapterId}">` +
                    createTextElement("h3", chapter.title) +
                    chapter.bodyHtml +
                    "</article>"
                );
            }).join("\n");
            contentsBodyHtml += `<ol>${bookContentList}</ol>\n`;
        }
        return bookHtml;
    }).join("\n");

    // Render table of contents
    novel += createSection(
        createTextElement("h2", "Contents"),
        `<nav>${contentsBodyHtml}</nav>`,
    );

    // Render books
    novel += booksHtml;

    console.log(`Word count: ${metrics.wordCount}.`);

    return createBook(title, novel);
}
