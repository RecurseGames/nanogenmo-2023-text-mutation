import nlp from 'compromise';
import plg from 'compromise-stats'
import Three from 'compromise/types/view/three';
import { getRandom, randInt } from '../utilities/random';
import { tryGetSuccess } from '../utilities/retries';
import { ensureTerminator, levenshteinDistance } from '../utilities/strings';

nlp.plugin(plg)

// const { default: nlp } = await import("compromise");
// const { default: plg } = await import("compromise-stats");

// let doc = nlp("he is paul's cat, but cats are good, and he hadn't really walked away quickly. the cow kisses the horse. it is the horse the cow kisses. she pulls their hair and kisses their face. and I say \"hey, what a wonderful kind of day\".");

// doc.text();
// doc.nouns();
// doc.sentences().toFutureTense();
// doc.nouns().toPlural();
// doc.nouns().random(); // "the horse", "the cow kisses" { text, terms, noun: { root, adjectives, isPLural, isSubordinate } }

// doc.terms().unique(); // hadn't -> had not, includes duplicates
// doc.termList(); // { text: "", implicit: "not", }
// doc.clauses(); // "but this is okay"
// doc.quotations();
// doc.verbs(); // "hadn't really walked away quickly", "is", etc.
// doc.adjectives().toSuperlative(); // this is the best
// doc.adjectives().toComparative(); // this is the better
// doc.adjectives().toAdverb(); // this is well

export class NlpSource {
    private readonly doc: Three;
    private clauses?: any[];

    constructor(text: string) {
        this.doc = nlp(text);
    }

    mutateOnce(text: string): string {
        const roll = randInt(1, 20);
        if (roll == 1) {
            return this.mutate(text, text => text.getVerbsModal()) || text;
        } else if (roll == 2) {
            return this.mutate(text, text => text.getVerbsParticle()) || text;
        } else if (roll <= 4) {
            return this.mutate(text, text => text.getVerbsInfinitive()) || text;
        } else if (roll <= 6) {
            return this.mutate(text, text => text.getVerbsPast()) || text;
        } else if (roll <= 8) {
            return this.mutate(text, text => text.getVerbsPresent()) || text;
        } else if (roll <= 11) {
            return this.mutate(text, text => text.getSingularNouns()) || text;
        } else if (roll <= 14) {
            return this.mutate(text, text => text.getPluralNouns()) || text;
        } else if (roll <= 17) {
            return this.mutate(text, text => text.getAdjectives()) || text;
        } else if (roll <= 20) {
            return this.mutate(text, text => text.getAdverbs()) || text;
        } else {
            throw new Error("unexpected failure");
        }
        // text = this.mutate(text, text => text.getSubjects()) || text;
        // text = this.mutate(text, text => text.getPredicates()) || text;
    }

    mutate(text: string, getElements: (text: NlpSource) => string[]): string | undefined {
        const textDocument = new NlpSource(text);
        const replaceableTexts = getElements(textDocument);
        if (!replaceableTexts.length) {
            return undefined;
        }

        const replacedText = getRandom(replaceableTexts);
        const replacementTexts = getElements(this).filter(replacementText => {
            const relativeLength = replacementText.length / replacedText.length;
            if (replacedText.length < 6) {
                return relativeLength < 2 && relativeLength > 0.5;
            }
            return relativeLength < 1.5 && relativeLength > 0.5;
        });
        if (replaceableTexts.length && replacementTexts.length) {
            return tryGetSuccess<string>(5, () => {
                const replacingText = getRandom(replacementTexts);
                if (replacedText != replacingText) {
                    return text.replace(replacedText, replacingText);
                } else {
                    return "";
                }
            });
        } else {
            return undefined;
        }
    }

    getPredicates(): string[] {
        return this.getClauses().map((e: any) => e.sentence.predicate).filter((e: any) => e);
    }

    getNouns(): string[] {
        return this.doc.nouns().json().map((e: any) => e.text);
    }

    getSingularNouns(): string[] {
        return this.getTerms(["Singular"]).map(e => e.text);
    }

    getPluralNouns(): string[] {
        return this.getTerms(["Plural"]).map(e => e.text);
    }

    debug() {
        console.log([...new Set(this.getTerms(["Infinitive"], ["Copula"]).map(e => `${e.text} (${[...(e.tags || [])].join()})`))].join("\n"));
    }

    // Cats "love"
    getVerbsInfinitive(): string[] {
        return this.getTerms(["Infinitive"], ["Copula"]).map(e => e.text);
    }

    // A cat "makes"
    getVerbsPresent(): string[] {
        return this.getTerms(["PresentTense"], ["Gerund", "Copula", "Infinitive"]).map(e => e.text);
    }

    // The cat is "thinking" / The "thinking" cats
    getVerbsPast(): string[] {
        return [
            ...this.doc.adjectives().json().map((e: any) => e.text),
            ...this.getTerms(["PastTense", "Gerund"], ["Copula", "Participle"]).map(e => e.text),
        ];
    }

    // It has "been"
    getVerbsParticle(): string[] {
        return this.getTerms(["Participle", "Particle"]).map(e => e.text);
    }

    // The cat/cats "should"
    getVerbsModal(): string[] {
        return this.getTerms(["Modal"]).map(e => e.text);
    }

    getAdverbs(): string[] {
        return this.doc.adverbs().json().map((e: any) => e.text);
    }

    getAdjectives(): string[] {
        return this.doc.adjectives().json().map((e: any) => e.text);
    }

    getVerbs(): string[] {
        return this.getClauses().map((e: any) => e.sentence.verb).filter((e: any) => e);
    }

    getSubjects(): string[] {
        return this.getClauses().map((e: any) => {
            return e.sentence.subject;
        }).filter((e: any) => e);
    }

    private getTerms(desirableTags?: string[], excludedTags?: string[]) {
        let terms = this.doc.termList();
        return terms.filter(term => {
            if (!term.tags) {
                return false;
            } else if (desirableTags && !desirableTags.some(tag => term.tags!.has(tag))) {
                return false;
            } else if (excludedTags && excludedTags.some(tag => term.tags!.has(tag))) {
                return false;
            } else {
                return true;
            }
        });
    }

    private getClauses() {
        return this.clauses ??= this.doc.clauses().json().map((clause: any) => {
            const clauseDocument = nlp(clause.text);
            clauseDocument.conjunctions().remove("");
            const clauseSentence = clauseDocument.sentences().json()[0];
            // if (!clauseSentence || !clauseSentence.sentence) {
            //     console.error("doc.text=" + this.doc.text());
            //     console.error("clause.text=" + clause.text);
            //     console.error("clauseDocument.text=" + clauseDocument.text());
            //     console.log(clauseDocument.sentences().json());
            //     throw "Oh";
            // }
            return clauseSentence;
        }).filter((clause: any) => clause);
    }

    getSentence(): string {
        return this.doc.sentences().random().text();
    }

    getFirstNames(): string[] {
        return [...this.doc.people().json()].map(e => e.person.firstName).filter(e => e);
    }

    getLastNames(): string[] {
        return [...this.doc.people().json()].map(e => e.person.lastName).filter(e => e);
    }

    getHonorifics(): string[] {
        return [...this.doc.people().json()].map(e => e.person.honorific).filter(e => e);
    }
}
