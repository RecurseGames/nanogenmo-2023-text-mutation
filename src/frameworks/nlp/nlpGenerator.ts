import nlp from 'compromise';
import plg from 'compromise-stats'
import Three from 'compromise/types/view/three';
import { getRandom, randInt } from '../utilities/random';
import { tryGetSuccess } from '../utilities/retries';
import { ensureTerminator, levenshteinDistance } from '../utilities/strings';
import { NlpSource } from './nlpSource';

nlp.plugin(plg)

export class NlpGenerator {
    private readonly sentenceSources: NlpSource[];
    private readonly mutationSources: NlpSource[];

    constructor(
        sources: NlpSource[],
        mutationSources?: NlpSource[],
    ) {
        this.sentenceSources = sources;
        this.mutationSources = mutationSources || sources;
    }

    getRandomSentence(): string {
        const sentence = getRandom(this.sentenceSources).getSentence();
        return this.mutateRandom(sentence);
    }
    
    getRandomParagraph(): string {
        const CHARS_PER_WORD = 5.2;
        const targetLength = CHARS_PER_WORD * (Math.random() < .3 ? randInt(40, 100) : randInt(100, 120));
        let paragraph = ensureTerminator(this.getRandomSentence(), ".");
        while (paragraph.length < targetLength) {
            paragraph += " " + ensureTerminator(this.getRandomSentence(), ".");
        }
        return paragraph;
    }

    private mutateRandom(text: string): string {
        let mutations = 0;
        let result = text;
        do {
            result = getRandom(this.mutationSources).mutateOnce(result);
            mutations++;
        } while (levenshteinDistance(text, result) < .4 * result.length && mutations < 20);
        return result;
    }
}
