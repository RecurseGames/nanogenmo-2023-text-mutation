export type Opinion = "love" | "hate" | "like" | "dislike" | undefined;

export const OPINIONS: Opinion[] = ["love", "like", undefined, "dislike", "hate"];

export type EntityId = string;

export type EntityType = "majorLocation" | "minorLocation" | "character";

export type EntityImportance = "central" | "supporting" | "peripheral";

export interface Entity {
    id: EntityId;
    type: EntityType;
    name?: string;
    importance: EntityImportance;
    opinions: {
        [key: EntityId]: EntityOpinion | undefined;
    };
    contents: EntityId[];
    containers: EntityId[];
};

export interface EntityOpinion {
    actual: Opinion;
    isRevealed?: boolean;
    knowers: EntityId[];
};

export interface MajorLocation extends Entity {

};

export interface MinorLocation extends Entity {

};

export interface World {
    entities: {
        [key: EntityId]: Entity | undefined;
    }
};
