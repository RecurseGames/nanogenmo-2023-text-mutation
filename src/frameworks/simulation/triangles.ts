import { off } from "process";
import { getEntities, getEntityIdentifier } from ".";
import { getRandom } from "../utilities/random";
import { Entity, EntityImportance, EntityOpinion, OPINIONS, Opinion, World } from "./types";
import { getContents } from "./location";
import { createRange } from "../utilities/arrays";

interface OpinionPair {
    subject: Entity;
    target: Entity;
    subjectOpinion: EntityOpinion;
    targetOpinion: EntityOpinion;
}

export function createDefaultOpinion(): EntityOpinion {
    return {
        actual: undefined,
        knowers: [],
    }
}

export function getOffsetOpinion(opinion: Opinion, offset: number): Opinion {
    const values: Record<Opinion & string, number> = {
        "love": 2,
        "like": 1,
        "dislike": -1,
        "hate": -2,
    };
    const value = opinion ? values[opinion] : 0;
    const offsetValue = value + offset;
    if (offsetValue >= 2) {
        return "love";
    } else if (offsetValue == 1) {
        return "like";
    } else if (offsetValue == 0) {
        return undefined;
    } else if (offsetValue == -1) {
        return "dislike";
    } else if (offsetValue <= -2) {
        return "hate";
    } else {
        throw new Error("Invalid offset.");
    }
}

export function getRandomOpinion(): Opinion {
    return getRandom(OPINIONS.filter(opinion => opinion));
}

export function getActualOpinion(subject: Entity, object: Entity): Opinion {
    const opinion = subject.opinions[object.id];
    return opinion ? opinion.actual : undefined;
}

export function getOpinionPair(subject: Entity, target: Entity): OpinionPair | undefined {
    if (subject === target) {
        return undefined;
    }

    const subjectOpinion = subject.opinions[target.id];
    if (subjectOpinion) {
        return {
            subject,
            target,
            subjectOpinion,
            targetOpinion: target.opinions[subject.id] || createDefaultOpinion(),
        };
    }

    const targetOpinion = target.opinions[subject.id];
    if (targetOpinion) {
        return {
            subject,
            target,
            subjectOpinion: createDefaultOpinion(),
            targetOpinion,
        };
    }

    return undefined;
}

export function getOpinionPairs(entities: Entity[]): OpinionPair[] {
    const pairs: OpinionPair[] = [];
    for (let i = 0; i < entities.length; i++) {
        for (let j = 0; j < entities.length; j++) {
            const pair = getOpinionPair(entities[i], entities[j]);
            if (pair) {
                pairs.push(pair);
            }
        }
    }
    return pairs;
}

export function getWorldOpinionPairs(world: World, subject?: Entity): OpinionPair[] {
    const pairs: OpinionPair[] = [];
    if (subject) {
        for (const target of getEntities(world)) {
            const pair = getOpinionPair(subject, target);
            if (pair) {
                pairs.push(pair);
            }
        }
    } else {
        for (const subject of getEntities(world)) {
            pairs.push(...getWorldOpinionPairs(world, subject));
        }
    }
    return pairs;
}

export function getOpinionTriangle(
    opinionPair: OpinionPair,
    entity: Entity,
): [OpinionPair, OpinionPair, OpinionPair] | undefined {
    const targetOpinionPair = getOpinionPair(opinionPair.target, entity);
    const entityOpinionPair = getOpinionPair(entity, opinionPair.subject);
    if (targetOpinionPair && entityOpinionPair) {
        return [opinionPair, targetOpinionPair, entityOpinionPair];
    }

    return undefined;
}

export function getOpinionTriangles(world: World, subject?: Entity): [OpinionPair, OpinionPair, OpinionPair][] {
    const triangles: [OpinionPair, OpinionPair, OpinionPair][] = [];
    if (subject) {
        const pairs = getWorldOpinionPairs(world, subject);
        for (const entity of getEntities(world)) {
            for (const pair of pairs) {
                const triangle = getOpinionTriangle(pair, entity);
                if (triangle) {
                    triangles.push(triangle);
                }
            }
        }
    } else {
        const entities = getEntities(world);
        for (let i = 0; i < entities.length; i++) {
            const subject = entities[i];
            for (let j = i + 1; j < entities.length; j++) {
                const target = entities[j];
                const pair = getOpinionPair(subject, target);
                if (pair) {
                    for (let k = j + 1; k < entities.length; k++) {
                        const triangle = getOpinionTriangle(pair, entities[k]);
                        if (triangle) {
                            triangles.push(triangle);
                        }
                    }
                }
            }
        }
    }
    return triangles;
}

export function renderOpinionPair(pair: OpinionPair): string {
    const { subjectOpinion, targetOpinion } = pair;
    const subject = getEntityIdentifier(pair.subject);
    const target = getEntityIdentifier(pair.target);
    if (subjectOpinion.actual == targetOpinion.actual) {
        return `${subject} and ${target} ${subjectOpinion.actual} each other.`;
    } else if (!subjectOpinion.actual) {
        return `${target} ${targetOpinion.actual}s ${subject}.`;
    } else if (!targetOpinion.actual) {
        return `${subject} ${subjectOpinion.actual}s ${target}.`;
    } else {
        return `${subject} ${subjectOpinion.actual}s ${target} but ${target} ${targetOpinion.actual}s ${subject}.`;
    }
}

export function isLoveOrHate(opinion: Opinion): boolean {
    return opinion === "love" || opinion === "hate";
}

export function getOpinionPairImportance({ 
    subject,
    target,
    subjectOpinion,
    targetOpinion,
}: OpinionPair): EntityImportance  {
    if (subject.importance === "peripheral" || target.importance === "peripheral") {
        return "peripheral";
    } else if (subject.importance === "central" && isLoveOrHate(subjectOpinion.actual) ||
        target.importance === "central" && isLoveOrHate(targetOpinion.actual)) {
        return "central";
    } else if (isLoveOrHate(subjectOpinion.actual) || isLoveOrHate(targetOpinion.actual)) {
        return "supporting";
    } else { 
        return "peripheral";
    }
}

export function compareImportance(a: EntityImportance, b: EntityImportance): number {
    if (a === b) {
        return 0;
    } else if (a === "central" || a === "supporting" && b === "peripheral"){
        return 1;
    } else {
        return -1;
    }
}

export function getImportanceWeightedLocation(world: World): Entity {
    const majorLocations = getEntities(world, "majorLocation");
    const pairs: OpinionPair[] = [];
    const pairLocations: Entity[] = [];
    for (const majorLocation of majorLocations) {
        for (const pair of getOpinionPairs(getContents(world, majorLocation))) {
            pairs.push(pair);
            pairLocations.push(majorLocation);
        }
    }
    if (!pairs.length) {
        throw new Error("Cannot get locations by importance, no locations are important");
    }

    const indices = createRange(pairs.length);
    const candidateLocations = new Set<Entity>();
    while (true) {
        const index = getImportanceWeighted(indices, i => getOpinionPairImportance(pairs[i]));
        const location = pairLocations[index];

        // We randomly pick pairs of entities based on their importance.
        // Once we've picked two from the same location, we decide that location is worth
        // focusing on. This means that we are biased towards locations with multiple
        // important relationships going on.
        if (candidateLocations.has(location)) {
            return location;
        } else {
            candidateLocations.add(location);
        }
    }
}

export function getImportanceWeighted<T>(items: T[], getImportance: (item: T) => EntityImportance) : T {
    if (!items.length) {
        throw new Error("Cannot get item from empty list.");
    }
    const desiredImportance: EntityImportance = Math.random() < .67 
        ? "central" 
        : (Math.random() < .67 ? "supporting" : "peripheral");
    const filteredItems = items.filter(i => getImportance(i) === desiredImportance);
    return filteredItems.length ? getRandom(filteredItems) : getRandom(items);
}
