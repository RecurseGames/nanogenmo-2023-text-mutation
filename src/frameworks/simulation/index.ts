import { createArray } from "../utilities/arrays";
import { getRandom, randInt } from "../utilities/random";
import { createHash } from "../utilities/strings";
import { getName } from "../../sources/names";
import { getSurname } from "../../sources/surnames";
import { putEntity } from "./location";
import { getOffsetOpinion, getRandomOpinion } from "./triangles";
import { Entity, EntityId, EntityImportance, EntityType, Opinion, World } from "./types";
import chalk from 'chalk';

export function createEntity(
    world: World,
    type: EntityType,
    importance: EntityImportance,
    id?: EntityId,
): Entity {
    if (id === undefined) {
        id = createHash();
    }

    return world.entities[id] = {
        id,
        type,
        importance,
        opinions: {},
        containers: [],
        contents: [],
    };
}

export function getRandomEntity(world: World, type: EntityType) : Entity {
    return getRandom(getEntities(world, type));
}

export function getImportance(importance: EntityImportance, offset: number): EntityImportance {
    offset += {
        central: 2,
        supporting: 1,
        peripheral: 0,
    }[importance];
    offset += Math.random() - .5;
    if (offset >= 1.5) {
        return "central";
    } else if (offset >= .5) {
        return "supporting";
    } else {
        return "peripheral";
    }
}

export function getEntities(world: World, type?: EntityType) : Entity[] {
    const entities = Object.values(world.entities).filter(e => e) as Entity[];
    return type ? entities.filter(e => e.type === type) : entities;
}

export function getEntity(world: World, id: EntityId) : Entity {
    const entity = getEntities(world).filter(e => e.id === id)[0];
    if (!entity) {
        throw new Error(`Entity not found: ${id}`);
    }
    return entity;
}

export function getEntityWeighted(world: World, type?: EntityType) : Entity {
    const results: Entity[] = [];
    for (const entity of getEntities(world, type)) {
        results.push(...{
            central: [entity, entity, entity, entity, entity],
            supporting: [entity, entity, entity],
            peripheral: [entity],
        }[entity.importance])
    }
    return getRandom(results);
}

export function addEntity(world: World, entity: Entity) {
    world.entities[entity.id] = entity;
}

export function createWorld(): World {
    const world = {
        entities: {}
    };

    const centralCharacterCount = randInt(3, 5);
    createArray(centralCharacterCount, () => createEntity(
        world,
        "character",
        "central",
    ));
    createArray(centralCharacterCount + randInt(-1, 1), () => createEntity(
        world,
        "character",
        "supporting",
    ));
    createArray(randInt(3, 5) + randInt(3, 5), () => createEntity(
        world,
        "character",
        "peripheral",
    ));

    createArray(randInt(1, 3), () => createEntity(
        world,
        "majorLocation",
        "central",
    ));
    createArray(randInt(1, 3), () => createEntity(
        world,
        "majorLocation",
        "supporting",
    ));
    createArray(randInt(5, 9), () => createEntity(
        world,
        "majorLocation",
        "peripheral",
    ));

    const characters = getEntities(world, "character");
    const characterLocationOptions = getEntities(world, "majorLocation");
    for (const character of characters) {
        const location = getRandom(characterLocationOptions);
        putEntity(character, location);
        characterLocationOptions.push(location);
        
        character.name = `${getName()} ${getSurname()}`;
    }

    for (let i = characters.length * 2; i > 0; i--) {
        const subject = getEntityWeighted(world, Math.random() < .2 ? undefined : "character");
        const target = getEntityWeighted(world, Math.random() < .2 ? undefined : "character");
        const subjectOpinion = subject.opinions[target.id];
        if (!subjectOpinion) {
            const opinion = getRandomOpinion();
            const inverseOpinion: Opinion = getRandom([opinion, getOffsetOpinion(opinion, randInt(-4, 4)), undefined]);

            subject.opinions[target.id] = {
                actual: opinion,
                knowers: [],
            };
            target.opinions[subject.id] = {
                actual: inverseOpinion,
                knowers: [],
            };
        }
    }

    return world;
}

export function getEntityTypeCount(world: World, type: EntityType): number {
    return getEntities(world, type).length;
}

export function getEntityIdentifier(entity: Entity): string {
    const identifier = entity.name || `${entity.type} ${entity.id}`;
    switch (entity.importance) {
        case "central":
            return chalk.yellowBright(identifier);
        case "supporting":
            return chalk.yellow(identifier);
        case "peripheral":
            return identifier;
    }
}

export function getEntityIndex(world: World, entityId: EntityId): number {
    const index = getEntities(world).findIndex(e => e.id === entityId);
    if (index == -1) {
        throw new Error("Entity not found");
    }
    return index;
}
