import { Entity, World } from "./types";

interface Plot {
    getEvents(): Generator<string, void, World>;
}

interface Scene {
    entities: Entity[];
}

class TestPlot implements Plot {
    *getEvents(): Generator<string, void, World> {
        let world = yield "";
        world = yield "";
        world = yield "";
    }
}