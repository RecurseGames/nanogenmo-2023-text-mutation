import { Entity, World } from "./types";
import { getEntity } from ".";


export function putEntity(entity: Entity, container: Entity): void {
    entity.containers.push(container.id);
    container.contents.push(entity.id);
}

export function removeEntity(entity: Entity, container?: Entity): void {
    if (container) {
        entity.containers = entity.containers.filter(e => e !== container.id);
        container.contents = container.contents.filter(e => e !== entity.id);
    }
}

export function getContainer(world: World, entity: Entity): Entity | undefined {
    if (entity.containers.length === 0) {
        return undefined;
    } else if (entity.containers.length === 1) {
        return getEntity(world, entity.containers[0]);
    } else {
        throw new Error("Entity has multiple locations.");
    }
}

export function getContents(world: World, entity: Entity): Entity[] {
    return entity.contents.map(id => world.entities[id]).filter(e => e) as Entity[];
}

export function moveEntity(world: World, entity: Entity, container: Entity): void {
    const oldContainer = getContainer(world, entity);
    removeEntity(entity, oldContainer);
    putEntity(entity, container);
}
