import { GlobalContext } from "./context";
import { OutputSource } from "./output";

export interface Section {
    context: GlobalContext;
    output: OutputSource<GlobalContext>;
}

export interface SectionSource {
    createSection: (previous: GlobalContext, next: GlobalContext) => Section | undefined;
}

export type SectionMutator = (sections: Section[]) => boolean;

export function tryInsertSection(sections: Section[], source: SectionSource, index: number): boolean {
    const previous = sections[index];
    const next = sections[index + 1];
    const section = source.createSection(previous.context, next.context);
    if (section) {
        sections.splice(index, 0, section);
        return true;
    } else {
        return false;
    }
}

export function tryMutate(
    sections: Section[], 
    mutator: SectionMutator, 
    attempts: number,
    maximumMutations: number = Number.POSITIVE_INFINITY,
): number {
    if (Number.isSafeInteger(attempts)) {
        let mutations = 0;
        while (attempts-- > 0 && mutations <= maximumMutations) {
            if (mutator(sections)) {
                mutations++;
            }
        }
        return mutations;
    } else {
        throw new Error("Attempts must be a safe integer.");
    }
}

export function getEarlyIndex(sections: Section[]) {
    return Math.floor(Math.pow(Math.random(), 2) * sections.length);
}

export function getLateIndex(sections: Section[]) {
    return sections.length - 1 - Math.floor(Math.pow(Math.random(), 2) * sections.length);
}

export function getMiddleIndex(sections: Section[]) {
    let randomFactor = Math.random() * 2 - 1;
    randomFactor = randomFactor * Math.abs(randomFactor) * .5 + .5;
    return randomFactor * sections.length;
}
