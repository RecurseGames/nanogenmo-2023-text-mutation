export type ContextValue = string | number;

export interface Context {
    [key: string]: undefined | ContextValue | ContextValue[] | Context;
}

export interface GlobalContext extends Context {
    temp?: ContextValue;
}
