export type OutputElement = string;

export type OutputFunction<Context> = (context: Context) => OutputElement | OutputElement[];

export type OutputSource<Context> = OutputFunction<Context> | OutputElement | OutputElement[];

export function getOutput<Context>(context: Context, source: OutputSource<Context>): OutputElement[] {
    if (typeof source === "function") {
        source = source(context)
    }

    if (typeof source === "string") {
        return [source];
    } else {
        return source;
    }
}
