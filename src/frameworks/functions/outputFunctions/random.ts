import { getRandom } from "../../utilities/random";
import { OutputSource, getOutput } from "../output";

export function createRandomOutput<Context>(...factories: OutputSource<Context>[]): OutputSource<Context> {
    return (context) => getOutput(context, getRandom(factories));
}
