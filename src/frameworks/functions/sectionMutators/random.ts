import { getRandom } from "../../utilities/random";
import { SectionMutator } from "../section";

export function createRandomMutator(...mutators: SectionMutator[]): SectionMutator {
    return sections => getRandom(mutators)(sections);
}
