import { createRange } from "../utilities/arrays";
import { getChoices } from "../utilities/random";

interface NovelState {
    [key: string]: string;
}

/**
 * A harms B, B tells C, C gets revenge on A
 * 
 * 
 * A harms B. A harms C. B and C team up and fight A.
 * A harms B. B tells C. C harms A.
 * A criticises B infront of C. C tells B. B harms A. A harms C.
 * A harms B. B tells A. Etc...
 * 
 * Too much like an event based system, might as well do that...
 * 
 * Scenes:
 * 1. Journey from A to B.
 * 2. 
 */

interface NovelSection {
    outcome: NovelState;
    isValid(state: NovelState): boolean;
    render(state: NovelState): string;
}

export function createState(initialState: NovelState, outcome: NovelState): NovelState {
    return {
        ...initialState,
        ...outcome,
    };
}

function validateSections(initialState: NovelState, sections: NovelSection[]): boolean {
    let state = initialState;
    for (let i = 0; i < sections.length; i++) {
        const section = sections[i];
        if (section.isValid(state)) {
            state = createState(state, section.outcome);
        } else {
            return false;
        }
    }
    return true;
}

class Novel {
    private readonly initialState: NovelState;
    private sections: NovelSection[] = [];

    constructor(initialState: NovelState) {
        this.initialState = initialState;
    }

    createSections(): NovelSection[] {
        return [...this.sections];
    }

    tryInsert(...sections: NovelSection[]): boolean {
        const indices = getChoices(createRange(this.sections.length), sections.length);
        if (indices) {
            indices.sort();
            const newSections = this.createSections();
            for (var i = 0; i < sections.length; i++) {
                newSections.splice(indices[i], 0, sections[i]);
            }
            if (validateSections(this.initialState, newSections)) {
                this.sections = newSections;
                return true;
            }
        }
        return false;
    }
}
